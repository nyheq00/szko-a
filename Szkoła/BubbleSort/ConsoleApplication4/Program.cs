﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Sortowanie_babelkowe
{
    class Program
    {

        static void Main(string[] args)
        {

            int bufor;
            bool posortowane;
            int zakres, liczba;
            string liczba1 = "nic";

            Random randomizer = new Random();               // Uzycie generator liczb losowych do uzupełnienia tablicy, która będzie posortowana

            while (!Int32.TryParse(liczba1, out zakres))     //Sprawdzenie czy wpisany znak jest liczbą, jeśli tak zwraca liczbe jako zmienną zakres
            {
                Console.Write("Podaj ilość liczb do posortowania: ");
                liczba1 = Console.ReadLine();
            }

            int[] tab = new int[zakres];                    // deklaracja tablicy o liczbie elementów równej zmiennej zakres
            Console.WriteLine("");

            for (int i = 0; i < zakres; i++)            // uzupełnienie tablicy liczbami losowymi
            {
                liczba = randomizer.Next(101);
                tab[i] = liczba;
                Console.Write(tab[i] + " ");            // wyświetlenie tablicy
            }

            Console.WriteLine("");
            Stopwatch timer = new Stopwatch();
            timer.Start();
            for (int i = 0; i < zakres - 1; i++)            // algorytm sortowania bąbelkowego
            {
                posortowane = true;
                for (int j = 0; j < zakres - 1; j++)
                {
                    if (tab[j] > tab[j + 1])                // jeśli dana liczba jest większa od kolejnej to zamień miejscami
                    {
                        bufor = tab[j];
                        tab[j] = tab[j + 1];
                        tab[j + 1] = bufor;
                        posortowane = false;
                    }
                }
                timer.Stop();

                if (posortowane) break;
                Console.WriteLine();
                Console.WriteLine("Krok[{0}]", i + 1);        // Wyświetl poszczególne kroki

                for (int k = 0; k < zakres; k++)
                {
                    Console.Write(tab[k] + " ");
                }
            }

            Console.WriteLine("\n");
            Console.Write("Po posortowaniu: ");             // Wyświetl liczby po posortowaniu

            for (int i = 0; i < zakres; i++)
            {
                Console.Write(tab[i] + " ");
            }
            Console.WriteLine(Environment.NewLine + "Wykonano w: " + timer.ElapsedMilliseconds + " ms.");
            Console.Read();
        }
    }
}