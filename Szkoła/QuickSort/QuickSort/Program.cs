﻿using System;
using System.Diagnostics;

namespace QuickSort
{
    class Program
    {
        public static void QuickSort(int[] less, int left, int right)
        {
            var l = left;
            var j = right;
            var desc = less[(left + right) / 2];
            while (l < j)
            {
                while (less[l] < desc) l++;
                while (less[j] > desc) j--;
                if (l <= j)
                {
                    var tmp = less[l];
                    less[l++] = less[j];
                    less[j--] = tmp;
                }
                if (left < j) QuickSort(less, left, j);
                if (l < right) QuickSort(less, l, right);
            }
        }

        static void Main(string[] args)
        {
            Random rand = new Random();
            string f;
            Console.WriteLine("Podaj ile liczb ma wylosowac");
            f = Console.ReadLine();
            var less = new int[Convert.ToInt32(f)];
            for (int i = 0; i < less.Length; i++)
            {
                less[i] = rand.Next(2000);
            }
            Console.WriteLine(Environment.NewLine + "Przed: ");
            Console.WriteLine(string.Join(" ", less));

            Stopwatch timer = new Stopwatch();
            timer.Start();
            QuickSort(less, 0, less.Length - 1);
            timer.Stop();

            Console.WriteLine(Environment.NewLine + "Po: ");
            Console.WriteLine(string.Join(" ", less));
            Console.WriteLine(Environment.NewLine + "Wykonano w: " + timer.ElapsedMilliseconds + " ms.");
            Console.ReadLine();
        }
    }
}