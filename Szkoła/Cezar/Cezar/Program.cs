﻿using System;
namespace Szyfr_Cezara
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wpisz tekst do szyfrowania");

            foreach (var z in Console.ReadLine())
            {
                Console.Write(Convert.ToChar((z - 'a' + 3) % ('z' - 'a' + 3) + 'a'));
            }
            Console.WriteLine();
            Console.ReadKey(true);
        }
    }
}
