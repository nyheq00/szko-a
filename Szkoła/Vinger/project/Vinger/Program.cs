﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinger
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            string output = String.Empty;
            Console.WriteLine("Wpisz tekst do szyfrowania");
            input = Console.ReadLine();
            string key = KeyGenerator.RandomString(input.Length);
            Dictionary<char, int> alphabet;
            Stopwatch timer = new Stopwatch();
            timer.Start();
            key = key.ToLower();
            if (key.Length >= input.Length)
            {
                alphabet = new Dictionary<char, int>();
                char c = 'a';
                alphabet.Add(c, 0);

                for (int i = 1; i < 26; i++)
                {
                    alphabet.Add(++c, i);
                }
                output = Encryptor.Crypt(input, key, alphabet);
            }
            timer.Stop();

            Console.Write(output);
            Console.Write(Environment.NewLine + "Wykonano w: " + timer.ElapsedMilliseconds + " ms.");
            Console.Read();
        }
    }
}
