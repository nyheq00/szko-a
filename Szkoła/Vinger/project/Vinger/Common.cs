﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinger
{
    public class Common
    {
        internal static int GetPosition(int textPosition, int keyPosition)
        {
            int result = 0;
            result = (textPosition + keyPosition) % 26;
            return result;
        }

        internal static string Crypt(string token, string Key, Dictionary<char, int> alphabetSorted)
        {
            string result = string.Empty;
            int textPosition, keyPosition, resPosition = 0;
            for (int i = 0; i < token.Length; i++)
            {
                textPosition = alphabetSorted[token[i]];
                keyPosition = alphabetSorted[Key[i]];
                resPosition = GetPosition(textPosition, keyPosition);
                result += alphabetSorted.Keys.ElementAt(resPosition);
            }
            return result;
        }
    }
}
