﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinger
{
    public class Encryptor
    {
        internal static int GetPosition(int textPosition, int keyPosition)
        {
            int result = 0;
            result = (textPosition + keyPosition) % 26;
            return result;
        }

        internal static string Crypt(string input, string Key, Dictionary<char, int> alphabetSorted)
        {
            string crypted = string.Empty;
            int textPosition, keyPosition, resPosition = 0;
            for (int i = 0; i < input.Length; i++)
            {
                textPosition = alphabetSorted[input[i]];
                keyPosition = alphabetSorted[Key[i]];
                resPosition = GetPosition(textPosition, keyPosition);
                crypted += alphabetSorted.Keys.ElementAt(resPosition);
            }
            return crypted;
        }
    }
}
